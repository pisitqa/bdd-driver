Scenario: Detail order page
Given I want to tap Driver app 
When  I am to the login success
Then  I click tab "กำลังส่ง"
And   I click tab "ดูรายการ"
And   I expect display status valid "กำลังส่ง"
And   I expect display order id valid "#4F21S"
And   I expect display kilometer valid "2.1 Km"
And   I expect display time valid "วันนี้ 13.22"
And   I expect display name surname valid "เฉลิมศรี จุรีพร"
And   I expect display phone number "094-9987879"
And   I expect display address "456 ซ.ประชาราษบำเพ็ญ 3 แขวงดินแดง.. "
And   I expect display food menu " 1x ปลากระพงสามรถ"
And   I expect display option menu "ไข่เค็ม x 2"
And   I expect display price "฿ 1,200.05"
And   I expect display total "฿ 1,200.05"
And   I expect display shipping "฿ 30"
And   I expect display total price "฿ 1,230.05"
And   I expect system will enable button "ส่งออเดอร์และชำระเงิน"

Scenario: Cash payment = price product
Given I want to tap Driver app 
When  I am to the login success
Then  I click tab "กำลังส่ง"
And   I click tab "ดูรายการ"
When  I click "ส่งออเดอร์และชำระเงิน"
Then  I expect system will display "Payment Page"
And   I expect system will enable button "<-" 
And   I expect system will defualt tab "เงินสด"
And   I expect system will display payment amount "฿ 1,230.05"
And   I expect system will enable textbox "จำนวนเงินที่รับมา"
And   I expect system will unable button "รับชำระ"
And   I expect system will unable tab "คิวอาร์โค้ด"

When  I click textbox "จำนวนเงินที่รับมา"
Then  I expect keybord will pop-up be "The number"
And   I expect system will fill "number only"
And   I fill amount "1,230.05"
And   I expect system will enable button "รับชำระ" after fill amount received

When  I delete amount received
Then  I expect system will delete numbers one by one 

When  I click button "รับชำระ"
Then  I expect system will display payment amount "฿ 1,230.05"
And   I expect system will display "รับเงินสด:       ฿ 1,230.05"
And   I expect system will display "เงินทอน :       ฿     0.00"
Then  I click "ยืนยันรับชำระ"
And   I expect system will pop-up message "ชำระเงินเรียบร้อย"
And   I expect system will pop-up message "เงินทอน"
And   I expect system will pop-up message "฿ 0.00"
And   I expect system will pop-up button "เสร็จสิน"
When  I click button "เสร็จสิ้น"
Then  I expect order will go display to tab "ส่งแล้ว"

Scenario: Paid > price product
When  I click button "รับชำระ"
Then  I expect system will display payment amount "฿ 1,230.05"
And   I expect system will display "รับเงินสด:       ฿ 1,500.00"
And   I expect system will display "เงินทอน :       ฿   269.95"
Then  I click "ยืนยันรับชำระ"
And   I expect system will pop-up message "ชำระเงินเรียบร้อย"
And   I expect system will pop-up message "เงินทอน"
And   I expect system will pop-up message "฿ 269.95"
And   I expect system will pop-up button "เสร็จสิน"
When  I click button "เสร็จสิ้น"
Then  I expect order will go display to tab "ส่งแล้ว"

Scenario: Paid < price product
When  I click textbox "จำนวนเงินที่รับมา"
Then  I fill amount "1,200.00"
And   I expect system will unable button "รับชำระ" after fill amount received

Scenario: Paid on pos
When  I click textbox "จำนวนเงินที่รับมา"
Then  I fill amount "1,200.00"
And   I expect system will paid success and driver status complete 

Scenario: Paid on online order
When  I click textbox "จำนวนเงินที่รับมา"
Then  I fill amount "1,200.00"
And   I expect system will paid success and pos status complete



