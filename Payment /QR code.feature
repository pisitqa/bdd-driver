Scenario: Paid by Qr code 
Given I want to tap Driver app 
When  I am to the login success
Then  I click tab "กำลังส่ง"
And   I click tab "ดูรายการ"
When  I click "ส่งออเดอร์และชำระเงิน"
Then  I expect system will display "Payment Page"
And   I expect system will enable button "<-" 
And   I expect system will defualt tab "เงินสด"

When  I click tab "คิวอาร์โค้ด"
Then  I expect system will unable button "เงินสด"
And   I expect system will display pic "Qr code"
And   I expect system will display "เลขที่อ้่างอิง : 2301293123109121"
And   I expect system will display "ยอดชำระ   ฿ 1,230.05"

When  customer scan paid "Qr code"
Then  I expect system will display "Payment amount that the bank app is correct"
And   I expect system will display "Date that the bank app is correct"
And   I expect system will display "Time that the bank app is correct"
Then  customer click paid success
And   I expect app driver display Pop up "ชำระเงินเรียบร้อย รายการชำระด้วยคิวอาร์โค้ดเสร็จสมบรูณ์"
And   I expect system will enable button "เสร็จสิ้น"
When  I click button "เสร็จสิ้น"
Then  I expect order will go display to tab "ส่งแล้ว"


Scenario: Scan paid Qr code already paid
Given I want to tap Driver app 
When  I am to the login success
Then  I click tab "กำลังส่ง"
And   I click tab "ดูรายการ"
When  I click "ส่งออเดอร์และชำระเงิน"
Then  I expect system will display "Payment Page"

When  customer scan the paid "Qr code" already paid
Then  I expect bank app alert "ไม่สามารชำระได้"


Scenario: Press button ส่งออเดอร์และชำระเงิน then press back then press ส่งออเดอร์และชำระเงิน again
Given I want to tap Driver app 
When  I am to the login success
Then  I click tab "กำลังส่ง"
And   I click tab "ดูรายการ"
When  I click "ส่งออเดอร์และชำระเงิน"
Then  I click "Back"
And   I click "ส่งออเดอร์และชำระเงิน"
And   I expect system will generate Qr code valid

Scenario: Paid on pos
When  I click textbox "จำนวนเงินที่รับมา"
Then  I fill amount "1,200.00"
And   I expect system will unable button "รับชำระ" 

Scenario: Paid on online order
When  I click textbox "จำนวนเงินที่รับมา"
Then  I fill amount "1,200.00"
And   I expect system will unable button "รับชำระ" 