
Scenario: Notification Cancel Order on Lock screen
Given I am to mobile
When  I press notification on mobile lock screen page
Then  I expect system will display order to tap "กำลังส่ง"
And   I expect display data valid

Scenario: Notification Cancel Order on Notification center 
Given I am to mobile
When  I press notification on mobile notification center  
Then  I expect system will display order to tap "กำลังส่ง"
And   I expect display data valid

Scenario: Notification Cancel Order on Placard in the driver app and other app
Given I am to mobile
When  I press notification on mobile placard
Then  I expect system will display order to tap "กำลังส่ง"
And   I expect display data valid
