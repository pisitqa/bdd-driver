Scenario: Notification Payment cash and Qr code on Lock screen 
Given I am to mobile
When  I press notification on mobile lock screen page
Then  I expect system will display order to tap "ส่งแล้ว"
And   I expect display data valid

Scenario: Notification Payment cash and Qr code on Notification center 
Given I am to mobile
When  I press notification on mobile notification center 
Then  I expect system will display order to tap "ส่งแล้ว"
And   I expect display data valid

Scenario: Notification Payment cash and Qr code on Placard in the driver app and other app
Given I am to mobile
When  I press notification on mobile placard
Then  I expect system will display order to tap "ส่งแล้ว"
And   I expect display data valid

 