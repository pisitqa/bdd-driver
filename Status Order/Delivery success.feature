Scenario: Refresh tab delivery success
Given I want to tap Driver app 
When  I click tab delivery success
Then  I slide down
And   I expect system will refresh order come show

Scenario: Refresh but no have order
Given I want to tap Driver app 
When  I click tab delivery success
Then  I slide down
And   I expect system will Alert "ไม่มีรายการออเดอร์"

Scenario: Validate item in order
Given I want to tap Driver app 
When  I click tab delivery success
Then  I click "ดูรายการ"
And   I expect display status valid "ส่งแล้ว"
And   I expect display order id valid "#4F21S"
And   I expect display kilometer valid "2.1 Km"
And   I expect display time valid "วันนี้ 13.22"
And   I expect displaysplay name surname valid "เฉลิมศรี จุรีพร"
And   I expect display phone number "094-9987879"
And   I expect display address "456 ซ.ประชาราษบำเพ็ญ 3 แขวงดินแดง.. "
And   I expect display food menu " 1x ปลากระพงสามรถ"
And   I expect display option menu "ไข่เค็ม x 2"
And   I expect display price "฿ 120"
And   I expect display total "฿ 120"
And   I expect display shipping "฿ 30 "
And   I expect display total price "฿ 150"