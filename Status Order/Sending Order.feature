Scenario: Refresh tab sending order
Given I want to tap Driver app 
When  I click tab sending order
Then  I slide down
And   I expect system will refresh order come show

Scenario: Refresh but no have order
Given I want to tap Driver app 
When  I click tab sending order
Then  I slide down
And   I expect system will Alert "ไม่มีรายการออเดอร์"

Scenario: Validate item in order
Given I want to tap Driver app 
When  I click tab sending order
Then  I click "ดูรายการ"
And   I expect display status valid "กำลังส่ง"
And   I expect display order id valid "#4F21S"
And   I expect shdisplayow kilometer valid "2.1 Km"
And   I expect display time valid "วันนี้ 13.22"
And   I expect display name surname valid "เฉลิมศรี จุรีพร"
And   I expect display phone number "094-9987879"
And   I expect display address "456 ซ.ประชาราษบำเพ็ญ 3 แขวงดินแดง.. "
And   I expect display food menu " 1x ปลากระพงสามรถ"
And   I expect display option menu "ไข่เค็ม x 2"
And   I expect display price "฿ 120"
And   I expect display total "฿ 120"
And   I expect display shipping "฿ 30 "
And   I expect display total price "฿ 150"
And   I expect display "โทร"
And   I expect display "นำทาง"
And   I expect display "ยกเลิก"
And   I expect display button "ส่งออเดอร์สำเร็จ"

Scenario: button navigate
Given I want to tap Driver app 
When  I click tab sending order
Then  I click "นำทาง"
And   I expect system will open maps

Scenario: button cancel order 
Given I want to tap Driver app 
When  I click tab sending order
Then  I click "ดูรายการ"
And   I click "ยกเลิก"
And   I expect system will cancel order and assing order to new Driver

Scenario: button call customer 
Given I want to tap Driver app 
When  I click tab sending order
Then  I click "ดูรายการ"
And   I click "โทร"
And   I expect system will go to tab delivery success

Scenario: button sending order success
Given I want to tap Driver app 
When  I click tab sending order
Then  I click "กดส่งออเดอร์สำเร็จ"
And   I expect system will go to tab delivery success