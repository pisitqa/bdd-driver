
Scenario: login app
Given I want to tap Driver app 
When  I am to the login page
Then  I fill email "zap4shabu.pos@gmail.com"
And   I fill password "Qwe123-/:"
And   I click "Login"
And   I expect can login success and display restaurant page

Scenario: Online
Given I want to tap Driver app 
When  I click button "Online"
Then  I expect system active and have item order come on

Scenario: Offline and try confirm order
Given I want to tap Driver app 
When  I click button "Offline"
Then  I expect system inactive 
And   I expect order can't come on

Scenario: Offline
Given I want to tap Driver app 
When  I click button "Offline"
Then  I expect system inactive  

Scenario: Pending order then offline
Given I want to tap Driver app 
When  I have order wait delivery
Then  I click button "Offline"
Then  I expect the system to not be able to log out.