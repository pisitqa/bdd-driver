
Scenario: login success
Given I want to tap Driver app 
When  I am to the login page
Then  I fill email "zap4shabu.pos@gmail.com"
And   I fill password "Qwe123-/:"
And   I click "Login"
And   I expect can login success and display restaurant page

Scenario: login unsuccess
Given I want to tap Driver app 
When  I fill email invalid
Then  I fill email "zap4shabu.pos@gmail.com"
And   I fill password "Qwe123-/:"
And   I click "Login"
And   I expect system alert "คุณใส่ Email or Password ไม่ถูกต้อง"

When  I fill password invalid
Then  I fill email "zap4shabu.pos@gmail.com"
And   I fill password "Qwe123-/:"
And   I click "Login"
And   I expect system alert "คุณใส่ Email or Password ไม่ถูกต้อง"

Scenario: Validate login button
Given I want to tap Driver app 
When  I am to the login page
Then  I fill email "Test@gmail.com"
And   I expect login button unable

When  I fill password "XXXX"
Then  I expect login button unable

When  I fill Email "Test@gmail.com"
Then  I fill password "XXXX"
And   I expect login button enable