
Scenario: logout
Given I want to tap Driver app 
When  I am to the login page
Then  I fill email "zap4shabu.pos@gmail.com"
And   I fill password "Qwe123-/:"
And   I click "Login"
Then  I click button "logout"
And   I expect can logout success and display login page

Scenario: pending order then logout
Given I want to tap Driver app 
When  I am to the login page
Then  I fill email "zap4shabu.pos@gmail.com"
And   I fill password "Qwe123-/:"
And   I click "Login"
Then  I click button "logout"
And   I expect can't logout and display alert message "ออกจากระบบไม่สำเร็จ"