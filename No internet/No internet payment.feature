Scenario: No internet then pay by CASH
Given I am to Driver app
When  I paid by "cash"
Then  Driver no internet
And   I expect system will alert message "ไม่สามารถชำระได้ กรุณาตรวจสอบเครื่อข่าย"
And   I expect order not change status

Scenario: No internet then pay by Qr code
Given I am to Driver app
When  I customer paid by "Qr code"
Then  Driver on internet
And   I expect system will alert message "กรุณาตรวจสอบเครื่อข่าย" 
And   I expect order change status 